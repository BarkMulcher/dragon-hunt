package DragonHunt;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JInternalFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Font;
import java.awt.Dimension;

public class SkillTree extends JFrame implements ActionListener{
	public static boolean choice = true;
	
	public static String at1, at2, at3;//holder Strings to get Attributes for ActionCommand
	
	String[] attributes ={
	"Tinkerer",
	"Intelligent",
	"Brute",
	"Loyal",
	"Treacherous",
	"Quiet",
	"Auspicious",
	"Charming",
	"Abominable",
	"Righteous",
	"Malicious",
	"Altruistic",
	"Avarice"
	};
	
	Font f1 = new Font("Serif", Font.BOLD, 40);
	Font f2 = new Font("Serif", Font.BOLD, 20);
	
	JScrollBar scroll = new JScrollBar();
	
	GridBagConstraints gbc = new GridBagConstraints();
	
	JPanel p = new JPanel();
	
	JLabel label1, label2, label3, instruct, a1, a2, a3, arch;
	
	JComboBox attribute1, attribute2, attribute3;
	
	JButton skillCatagories[] = new JButton[GlobalVars.skillCats.length];
	JLabel skillLabel[] = new JLabel[GlobalVars.skillCats.length];
	
	public SkillTree(){
		super("Skill Tree");
		if(choice == true){
			
			setSize(650,400);
			setResizable(false);
			p.setLayout(new GridBagLayout());
			
			gbc.insets = new Insets(5,5,5,5);
			gbc.weightx = .5;
			gbc.weighty = .5;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			
			instruct = new JLabel("Please choose your three attributes", SwingConstants.CENTER);
			gbc.gridy =0;
			gbc.gridx = 0;
			gbc.gridwidth = 3;
			instruct.setFont(f1);
			p.add(instruct, gbc);
			
			label1 = new JLabel("Attribute 1", SwingConstants.CENTER);
			gbc.gridy = 1;
			gbc.gridx = 0;
			gbc.gridwidth = 1;
			label1.setFont(f2);
			p.add(label1, gbc);
			
			label2 = new JLabel("Attribute 2", SwingConstants.CENTER);
			gbc.gridx = 1;
			gbc.gridwidth = 1;
			label2.setFont(f2);
			p.add(label2, gbc);
			
			label3 = new JLabel("Attribute 3", SwingConstants.CENTER);
			gbc.gridx = 2;
			gbc.gridwidth = 1;
			label3.setFont(f2);
			p.add(label3, gbc);
			
			attribute1 = new JComboBox(attributes);
			attribute1.addActionListener(this);
			gbc.gridy = 2;
			gbc.gridx = 0;
			gbc.gridwidth = 1;
			p.add(attribute1, gbc);
			
			attribute2 = new JComboBox(attributes);
			gbc.gridx = 1;
			gbc.gridwidth = 1;
			p.add(attribute2, gbc);
			
			attribute3 = new JComboBox(attributes);
			gbc.gridx = 2;
			gbc.gridwidth = 1;
			p.add(attribute3, gbc);
			
			add(p);
			setVisible(true);
		}else{
			setMinimumSize(new Dimension(750,400));
			setMaximumSize(new Dimension(750, 1000));
			setResizable(true);
			p.setLayout(new GridBagLayout());
			
			gbc.insets = new Insets(5,5,5,5);
			gbc.weightx = .5;
			gbc.weighty = .5;
			
			arch = new JLabel("Your Chosen Attributes Are", SwingConstants.CENTER);
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.gridwidth = 5;
			arch.setFont(f1);
			p.add(arch, gbc);
			
			a1 = new JLabel(at1, SwingConstants.CENTER);
			gbc.gridx = 0;
			gbc.gridy = 1;
			gbc.gridwidth = 2;
			a1.setFont(f2);
			p.add(a1,gbc);
			
			a2 = new JLabel(at2, SwingConstants.CENTER);
			gbc.gridx = 2;
			gbc.gridy = 1;
			gbc.gridwidth = 1;
			a2.setFont(f2);
			p.add(a2,gbc);
			
			a3 = new JLabel(at3, SwingConstants.CENTER);
			gbc.gridx = 3;
			gbc.gridy = 1;
			gbc.gridwidth = 2;
			a3.setFont(f2);
			p.add(a3,gbc);
			
			for(int i=0; i<GlobalVars.skillCats.length; i++){
				skillCatagories[i] = new JButton(GlobalVars.skillCats[i]);
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.gridx = i%5;
				gbc.gridy = ((i/5)*2)+2;
				gbc.gridwidth = 1;
				skillCatagories[i].addActionListener(this);
				p.add(skillCatagories[i], gbc);
			}
			
			for (int i=0;i<GlobalVars.skillCats.length; i++){
				skillLabel[i] = new JLabel("Lvl " + GlobalVars.sCLevels[i], SwingConstants.CENTER);
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.gridx = i%5;
				gbc.gridy = ((i/5)*2)+3;
				gbc.gridwidth = 1;
				p.add(skillLabel[i], gbc);
			}
			
			JScrollPane sp = new JScrollPane(p);
			sp.getVerticalScrollBar().setUnitIncrement(10);
			add(sp);
			setVisible(true);
		}
	}
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		if(src.equals(attribute1)){
			at1 = String.valueOf(attribute1.getSelectedItem());
			GlobalVars.attribute1 =at1;
			attribute2.addActionListener(this);
			attribute1.removeActionListener(this);
		}else if(src.equals(attribute2)){
			at2 = String.valueOf(attribute2.getSelectedItem());
			GlobalVars.attribute2=at2;
			attribute3.addActionListener(this);
			attribute2.removeActionListener(this);
		}else if (src.equals(attribute3)){
			at3 = String.valueOf(attribute3.getSelectedItem());
			GlobalVars.attribute3 = at3;
			choice = false;
			new SkillTree();
			dispose();
		}else if (src.equals(skillCatagories[0])){
			new Crafting();
		}
	}
	
}
package DragonHunt;

import java.lang.Runnable;
import java.lang.Thread;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.BorderFactory; 
import javax.swing.border.Border;
import javax.swing.JScrollPane;
import javax.swing.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;

import java.applet.Applet;

public class DragonHunt extends JFrame implements ActionListener{
	//Instantiates other class for later use
	
	
	
	String[] attacks ={//an array for the abilities combo box
		"Basic Attack"
	};
	
	Font f = new Font("Serif", Font.BOLD, 15);
	
	GridBagConstraints gbc = new GridBagConstraints();
	
	JPanel p = new JPanel();//main panel that other panels are added to
	
	JPanel main = new JPanel();//main text output window
	JPanel input = new JPanel();//interactive panel for user
	JPanel tracker = new JPanel();//tracks location and time 
	JPanel info = new JPanel();//shows player stats
	
	
	JLabel empty1 = new JLabel("Place Holder");//place holders until i implement the data fo rthese areas
	JLabel age,day,hour,location,ageV,dayV,hourV,locationV;
	
	JTextField response = new JTextField();
	
	JComboBox abilities = new JComboBox(attacks);//drop down box for abilities
	
	//buttons to open the other JFrames
	JButton inv = new JButton("Inventory");
	JButton skills = new JButton("Skill Tree");
	JButton menu = new JButton("Menu");
	JButton cont = new JButton("Continue");
	
	public static void main(String[] args){
		Items.createItems();
		new StartScreen();
		
	}
	
	public DragonHunt(){
		super ("Dragon Hunt");
		setSize(750,750);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		p.setLayout(new BorderLayout());// sets the layout of the main panel
		
		//tracker panel
		//panel setup
		tracker.setLayout(new GridBagLayout());
		gbc.fill = GridBagConstraints.VERTICAL;
		gbc.weighty = .5;
		gbc.weightx = .5;
		gbc.gridheight= 3;
		gbc.gridwidth = 1;
		//year label
		age = new JLabel("Age: ");
		gbc.gridx=6;
		gbc.gridy= 0;
		tracker.add(age, gbc);
		//day label
		day = new JLabel("Day: ");
		gbc.gridx=4;
		tracker.add(day, gbc);
		//time label
		hour = new JLabel("Time: ");
		gbc.gridx=2;
		tracker.add(hour, gbc);
		//location label
		location = new JLabel("Location: ");
		gbc.gridx=0;
		tracker.add(location, gbc);
		//age Variable label
		gbc.gridx=7;
		gbc.gridy= 0;
		tracker.add(GlobalVars.ageLabel, gbc);
		//day Variable label
		gbc.gridx=5;
		tracker.add(GlobalVars.dayLabel, gbc);
		//time Variable label
		gbc.gridx=3;
		tracker.add(GlobalVars.timeLabel, gbc);
		//location Variable label
		gbc.gridx=1;
		tracker.add(GlobalVars.locationLabel, gbc);
		//dimension set
		tracker.setPreferredSize(new Dimension(700,100));
		p.add(tracker, BorderLayout.NORTH);
		
		//info Panel
		//panel setup
		info.setLayout(new GridBagLayout());
		gbc.weighty = .5;
		gbc.anchor= GridBagConstraints.CENTER;
		gbc.gridheight= 3;
		gbc.fill = GridBagConstraints.VERTICAL;
		info.add(empty1, gbc);
		
		info.setPreferredSize(new Dimension(700,100));
		
		p.add(info, BorderLayout.SOUTH);
		
		//input panel
		//panel setup
		input.setLayout(new GridBagLayout());
		
		gbc.weighty= .5;
		gbc.insets = new Insets(5,10,5,10);
		gbc.gridheight= 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridy = 3;
		input.add(response,gbc);
		gbc.gridy = 0;
		input.add(inv, gbc);
		gbc.gridy = 1;
		input.add(skills, gbc);
		gbc.gridy = 2;
		input.add(menu, gbc);
		gbc.gridy = 4;
		input.add(abilities, gbc);
		gbc.gridy = 10;
		input.add(cont, gbc);
		//action listeners for the buttons
		inv.addActionListener(this);
		skills.addActionListener(this);
		menu.addActionListener(this);
		cont.addActionListener(this);
		
		p.add(input, BorderLayout.WEST);

		//main panel
		//panel setup
		main.setLayout(new GridLayout(1,1));
		main.add(GlobalVars.hud);
		GlobalVars.hud.setFont(f);
		GlobalVars.hud.setEditable(false);
		p.add(main);
		
		add(p);
		setVisible(true);
	}
	
	//action listeners
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		//start
		if (src.equals(inv)){//opens inventory panel
			new InventoryMenu();
		}else if (src.equals(skills)){//opens skills panel
			new SkillTree();
		}else if(src.equals(menu)){//opens menu panel
			new Menu();
		}else if (src.equals(cont)){//continues the story progression
			GlobalVars.storyArc++;
			(new Story()).start();
		}
	}
}
package DragonHunt;
import java.util.*;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JInternalFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Insets;

public class InventoryMenu extends JFrame implements ActionListener{
	public static List<Item> inventory = new ArrayList<Item>();
	JPanel p = new JPanel();//panel to contain everything
	GridBagConstraints gbc = new GridBagConstraints();
	JButton item[] = new JButton[40];
	int itemsInInventory = 0;//counts the number of items in the inventory
	
	public InventoryMenu(){
		super("Inventory");
		setMinimumSize(new Dimension (800,500));
		setResizable(true);
		
		
		
		for (int i = 0;i<40;i++){
			item[i] = new JButton();
			gbc.gridx = i%6;
			gbc.gridy = (i/6)+1;
			item[i].addActionListener(this);
			p.add(item[i], gbc);
		}
		
		JScrollPane sp = new JScrollPane(p);
		add(sp);
		setVisible(true);
	}
	
	
	public void addItem(int x){
		if (Items.getItems(x).isResrc()==true){
			if(Items.getItems(x).getQuantity()<=0){
				Items.getItems(x).incQuantity();
				inventory.add(Items.getItems(x));
			} else{
				Items.getItems(x).incQuantity();
			}
		}else if(Items.getItems(x).isResrc()==false){
		inventory.add(Items.getItems(x));
		}
		for(int i = 0; i<inventory.size();i++)	
			System.out.print(inventory.get(i));
		updateInv();
	}
	public void removeItem(int x){
		inventory.remove(Items.getItems(x));
		updateInv();
	}
	public void updateInv(){
		
	}
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		for(int i = 0; i<inventory.size(); i++)
			if (src.equals(item[i])){
				JOptionPane.showMessageDialog(null, "Name: "+ inventory.get(i).getName() +"\nDescription: " + inventory.get(i).getDescription() + "\nQuantity: " + Integer.toString(inventory.get(i).getQuantity()));
			}
	}
}
class Items{
	private static List<Item> items = new ArrayList<Item>();
	//adds blank Item objects to the master list to later be referenced by ID number and filled in
	public static void createItems(){
		for (int i = 0; i < 100; i++){
			items.add(new Item());
		}
		items.get(0).setStats(true, "Rock", "This is a rock");
		items.get(1).setStats(false, "Wooden Sword", "Basic Weighted sword, for training only.");
	}
	public static Item getItems(int x){
		return items.get(x);
	}
	
}
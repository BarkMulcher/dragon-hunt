package DragonHunt;

import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.lang.Thread;
import java.lang.Runnable;

public class GlobalVars{
	public static JTextArea hud = new JTextArea();//main hud for the game story is read through here
	public static int storyArc=0;
	public static boolean vampire = false;
	public static boolean wereType = false;
	public static String[] skillCats ={//array of skill catagories
		"Crafting",
		"Small Arms",
		"Large Weapons",
		"Exotic Weapons",
		"Archery",
		"Leadership",
		"Merchant",
		"Exploration",
		"Magic",
		"Beast Lore",
		"Vamprism",
		"Concentration"
	};
	
	public static String[] sCLevels = new String[skillCats.length];
	
	public void resetLvlAll(){
		for(int i = 0; i<100; i++){
			if(i<skillCats.length){//resets the level of all skill catagories
			sCLevels[i] = "1";
			}
			if (i<crafting.length){//resest the level of all the crafting skills
			cLevels[i] = "1";
			}
		}
	}
	
	public static String[] crafting = {//array listing all of the skills in the crafting catagory
		"Refining",
		"Weapon Smithing",
		"Wizard Smithing",
		"Armour Forging",
		"Wizard Forging",
		"Woodworking",
		"Runic Woodworking",
		"Tailoring",
		"Farming",
		"Cooking",
		"Cartography",
		"Fletching",
		"Jeweller",
		"Enchanting",
		"Potion Making",
		"Alchemy"
	};
	public static String[] cLevels = new String[crafting.length];
	
	
	
	public static JLabel locationLabel = new JLabel();
	private static String location = "";
	public void setLocation(String x, int y){
		setTime(y);
		this.location = x;
		locationLabel.setText(getLocation());
	}
	public String getLocation(){
		return location;
	}
	
	public static JLabel timeLabel = new JLabel();
	private static String time = "0";
	public void setTime(int x){
		int y = Integer.parseInt(getTime());
		y += x;
		y = y%24;
		setDay(y/24);
		String z = Integer.toString(y);
		this.time = z;
		timeLabel.setText(getTime());
	}
	public String getTime(){
		return time;
	}
	
	public static JLabel ageLabel = new JLabel();
	private static String age = "16";
	public void setAge(int x){
		int y = Integer.parseInt(getAge());
		y += x;
		String z = Integer.toString(y);
		this.age = z;
		ageLabel.setText(getAge());
	}
	public String getAge(){
		return age;
	}
	
	public static JLabel dayLabel = new JLabel();
	private static String day = "1";
	public void setDay(int x){
		int y = Integer.parseInt(getDay());
		y+= x;
		y = y%365;
		setAge(y/365);
		String z = Integer.toString(y);
		this.day = z;
		dayLabel.setText(getDay());
	}
	public String getDay(){
		return day;
	}
	
	public static String attribute1="";
	public static String attribute2="";
	public static String attribute3="";
	
	public static void fight(){
		Story S = new Story();
		int x = storyArc+1;
		storyArc = -1;
		(new Story()).start();
		storyArc = x;
		(new Story()).start();
		
	}
}
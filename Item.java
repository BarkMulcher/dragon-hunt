package DragonHunt;


public class Item {
	private boolean resource;
	public boolean isResrc(){
		return resource;
	}
	
	private int quantity=0;
	public int getQuantity(){
		return quantity;
	}
	public void incQuantity(){
		this.quantity++;
	}
	
	private String description;
	public String getDescription(){
		return description;
	}
	
	
	private String name;
	public String getName(){
		return name;
	}
	
	public void setStats(boolean x, String y, String z){
		this.resource = x;
		this.name= y;
		this.description = z;
	}
	
}
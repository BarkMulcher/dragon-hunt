package DragonHunt;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Menu extends JFrame implements ActionListener{
	JPanel p = new JPanel();
	
	JButton save = new JButton("Save Game");
	JButton close = new JButton("Close Menu");
	JButton load = new JButton("Load Game");
	JButton exit = new JButton("Exit Game");
	
	GridBagConstraints gbc = new GridBagConstraints();
	
	public Menu(){
		super("Menu");
		setSize(100,250);
		setResizable(false);
		p.setLayout(new GridBagLayout());
		
		//layout menu buttons buttons
		gbc.insets= new Insets(5,1,5,1);
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		p.add(save, gbc);
		gbc.gridy = 1;
		p.add(load, gbc);
		gbc.gridy = 2;
		p.add(exit, gbc);
		gbc.gridy = 3;
		p.add(close, gbc);
		//adding action listeners to buttons
		save.addActionListener(this);
		load.addActionListener(this);
		exit.addActionListener(this);
		close.addActionListener(this);
		
		add(p);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		if (src.equals(save)){
			dispose();
		}else if (src.equals(load)){
			new LoadMenu();
			dispose();
		}else if (src.equals(exit)){
			System.exit(0);
		}else{
			dispose();
		}
	}
}
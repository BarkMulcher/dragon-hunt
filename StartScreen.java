package DragonHunt;

import java.lang.Runnable;
import java.lang.Thread;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class StartScreen extends JFrame implements ActionListener{
	GlobalVars gv = new GlobalVars();
	JPanel p = new JPanel();
	JPanel starting = new JPanel();
	
	JLabel open1 = new JLabel("Welcome to Dragon Hunt", SwingConstants.CENTER);
	JLabel open2 = new JLabel("Press Start", SwingConstants.CENTER);
	
	JButton start = new JButton("Start");
	
	
	public StartScreen(){
		super();
		setSize(300,400);
		setResizable(false);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		//starting panel
		starting.setLayout(new GridLayout(3,1));
		starting.add(open1);
		starting.add(open2);
		starting.add(start);
		start.addActionListener(this);
		p.add(starting);
		add(p);
		
		setVisible(true);
	}
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		//start
		if(src.equals(start)){
			dispose();
			new DragonHunt();
			(new Story()).start();
			gv.resetLvlAll();
			gv.setLocation("Forest",12);
		}
	}
}
package DragonHunt;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.Font;

public class Crafting extends JFrame implements ActionListener{
	JPanel p = new JPanel();
	
	GridBagConstraints gbc = new GridBagConstraints();
	
	Font f1 = new Font("Serif", Font.BOLD, 40);
	
	JButton skills[] = new JButton[GlobalVars.crafting.length];
	
	JLabel skillLevels[] = new JLabel[GlobalVars.crafting.length];
	JLabel arch;
	public Crafting(){
		super("Crafting");
		
		setMinimumSize(new Dimension(750,400));
		setMaximumSize(new Dimension(750, 1000));
		setResizable(true);
		p.setLayout(new GridBagLayout());
		
		gbc.insets = new Insets(5,5,5,5);
		gbc.weightx = .5;
		gbc.weighty = .5;
		
		arch = new JLabel("Crafting", SwingConstants.CENTER);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 5;
		arch.setFont(f1);
		p.add(arch, gbc);
		
		
		for(int i=0; i<GlobalVars.crafting.length; i++){
			skills[i] = new JButton(GlobalVars.crafting[i]);
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.gridx = i%5;
			gbc.gridy = ((i/5)*2)+2;
			gbc.gridwidth = 1;
			skills[i].addActionListener(this);
			p.add(skills[i], gbc);
		}
		
		updateLvls();
		
		JScrollPane sp = new JScrollPane(p);
		sp.getVerticalScrollBar().setUnitIncrement(10);
		add(sp);
		setVisible(true);
	}
	public static void updateLvls(){
		for (int i=0;i<GlobalVars.crafting.length; i++){
			skillLevels[i] = new JLabel("Lvl " + GlobalVars.cLevels[i], SwingConstants.CENTER);
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.gridx = i%5;
			gbc.gridy = ((i/5)*2)+3;
			gbc.gridwidth = 1;
			p.add(skillLevels[i], gbc);
		}
	}
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		if (src.equals(skills[0])){
			JOptionPane.showMessageDialog(null, "filler");
		}
	}
}